import React from "react";
import "./App.css";
import "./Header.css";
import styled from "styled-components";
import { withRouter } from "react-router-dom";
import Logo from "./logo.png";

const Header = (props) => {
  const topNavigationContents = ["CONTACTS", "LOCATIONS", "SCHEDULE"];

  function navigationPath(contents) {
    let component = "";
    switch (contents) {
      case "SCHEDULE":
        props.history.push("/Schedule");
        break;
      case "LOCATIONS":
        props.history.push("/Locations");
        break;
      case "CONTACTS":
        props.history.push("/Contacts");
        break;
      default:
        return component;
    }
  }

  const HoverText = styled.div`
    color: #5e5e5e;
    :hover {
      color: darkblue;
      cursor: pointer;
    }
  `;

  const navigation = topNavigationContents.map((contents) => {
    return (
      <HoverText
        className="top_Navigation"
        onClick={() => navigationPath(contents)}
      >
        {" "}
        {contents} &nbsp; / &nbsp;{" "}
      </HoverText>
    );
  });

  return (
    <div className="App-header">
      <div className="row">
        <span className="col-md-4 col-sm-6 col-xs-8">
          <img src={Logo} alt="website logo" />
        </span>
        <div className="col-md-8 col-sm-6 col-xs-4">{navigation}</div>
      </div>
    </div>
  );
};

export default withRouter(Header);
