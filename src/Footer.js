import React from "react";

import "./Footer.css";
import Logo from "./logo.png";

const Footer = (props) => {
  return (
    <div className="FooterStatic">
      <div className="FooterContent"> 
      <div className = "col-md-4 col-sm-4 widget">
    <h4 className = "WidgetTitle">  <b>ABOUT SILOAM INDIA</b></h4>
<div className = "textWidget">
<img src={Logo} alt="website logo" />
</div>

Siloam India is a family passionate about glorifying God, being discipled in the teachings of Jesus Christ and seeing lives, families, churches and nations transformed through the power of the Holy Spirit.
      </div>
      </div>
      <div className="FooterBottom">
        <div className="copyrights-col-left col-md-6 col-sm-6">
          <p> &copy;2012 SILOAM INDIA. All Rights Reserved </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
