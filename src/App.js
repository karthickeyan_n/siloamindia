import React, { Component } from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";

import "./App.css";
import SiloamIndia from "./SiloamIndia";
import Schedule from "./Schedule";
import Locations from "./Locations";
import Contacts from "./Contacts";
import Footer from "./Footer"

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
         <SiloamIndia/>
          <Switch>
            {/* <Route path="/"  component={SiloamIndia} /> */}
            <Route path="/Schedule" component = {Schedule} />
            <Route path="/Locations" component={Locations} />
            <Route path="/Contacts" component={Contacts} />
          </Switch>
          <Footer/>
        </BrowserRouter>

        <h2> The Best website is yet to be built </h2>
      </div>
    );
  }
}

export default App;
