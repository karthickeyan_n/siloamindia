import React, { Component } from "react";
import "./App.css";
import "./Header.css";

class Information extends Component {
  render() {
    return (
      <div className="bar">
        <ul>
          <li>Home</li>
          <li>Who we are</li>
          <li>Ministries</li>
          <li>Media</li>
          <li>Events</li>
          <li>New Here</li>
          <li>Prayer request</li>
          <li>Give</li>
        </ul>
      </div>
    );
  }
}
export default Information;
